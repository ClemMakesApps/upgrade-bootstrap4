const program = require('commander');
const replace = require('replace-in-file');
const escapeRegex = require('escape-string-regexp');
const migrations = require('./migrations.json');

program
  .version('0.0.1')
  .usage('[options] <directory glob>')
  .parse(process.argv);

console.log('************************');
console.log('\nBootstrap 4 upgrade tool\n');
console.log('This tool does not migrate the following (please check your codebase):');
console.log('* .form-horizontal has been removed');
console.log('* .item should change to .carousel-item');
console.log('* .col-*-offset-* should change to .offset-*');
console.log('* .col-*-push-* should change to .order-*-2');
console.log('* .col-*-pull-* should change to .order-*-1');

try {
  migrations
    .forEach((migration) => {
      const { regex } = migration;
      const escaped = regex || escapeRegex(migration.from).replace(/\@/g, '\@');

      const changes = replace.sync({
        files: program.args,
        from: new RegExp(escaped, 'g'),
        to: migration.to,
      });

      console.log(`\n${migration.from} => ${migration.to}`);
      changes.forEach(file => console.log(file));
    });
} catch (error) {
  console.log(`Error occurred: ${error}`);
}
